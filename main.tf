resource "google_compute_network" "hol" {
  name                    = "vpc-hol"
  auto_create_subnetworks = false
  
  depends_on = [ 
    google_project_service.gcp_services
  ]
}

resource "google_compute_network" "hol2" {
  name                    = "vpc-hol2"
  auto_create_subnetworks = false
  
  depends_on = [ 
    google_project_service.gcp_services
  ]
}

resource "google_compute_subnetwork" "hol" {
  name          = "subnet-hol"
  ip_cidr_range = "10.0.0.0/24"
  region        = var.gcp_region
  network       = google_compute_network.hol.id
  private_ip_google_access = true
  secondary_ip_range {
    range_name    = "pod"
    ip_cidr_range = "10.100.0.0/14" 
  }
  secondary_ip_range {
    range_name    = "svc"
    ip_cidr_range = "10.200.0.0/20" 
  }
}

resource "google_compute_subnetwork" "hol2" {
  name          = "subnet-hol2"
  ip_cidr_range = "10.1.0.0/24"
  region        = var.gcp_region
  network       = google_compute_network.hol2.id
  private_ip_google_access = true
  secondary_ip_range {
    range_name    = "pod"
    ip_cidr_range = "10.104.0.0/14" 
  }
  secondary_ip_range {
    range_name    = "svc"
    ip_cidr_range = "10.201.0.0/20" 
  }
}

resource "google_compute_global_address" "service_range" {
  name          = "test-add"
  purpose       = "VPC_PEERING"
  address_type  = "INTERNAL"
  prefix_length = 20
  network       = google_compute_network.hol.id
  depends_on = [ 
    google_compute_subnetwork.hol
  ]

}

resource "google_compute_global_address" "service_range2" {
  name          = "test-add2"
  purpose       = "VPC_PEERING"
  address_type  = "INTERNAL"
  prefix_length = 24
  network       = google_compute_network.hol2.id
  depends_on = [ 
    google_compute_subnetwork.hol2
  ]

}


resource "google_service_networking_connection" "private_service_connection" {
  network                 = google_compute_network.hol.id
  service                 = "servicenetworking.googleapis.com"
  reserved_peering_ranges = [google_compute_global_address.service_range.name]
}

resource "google_service_networking_connection" "private_service_connection2" {
  network                 = google_compute_network.hol2.id
  service                 = "servicenetworking.googleapis.com"
  reserved_peering_ranges = [google_compute_global_address.service_range2.name]
}

data "google_project" "project" {
  provider = google-beta
  project_id = var.gcp_project_id
}

resource "google_project_service" "gcp_services" {
  for_each = toset(var.gcp_service_list)
  project = var.gcp_project_id
  service = each.key
}
#
# resource "google_workstations_workstation_cluster" "default" {
#   provider               = google-beta
#   workstation_cluster_id = "workstation-cluster"
#   network                = google_compute_network.hol.id
#   subnetwork             = google_compute_subnetwork.hol.id
#   location               = var.gcp_region
#   project                = var.gcp_project_id

#   labels = {
#     "label" = "key"
#   }

#   annotations = {
#     label-one = "value-one"
#   }

#   depends_on = [ 
#     google_project_service.gcp_services
#   ]
# }

# resource "google_workstations_workstation_config" "default" {
#   provider               = google-beta
#   workstation_config_id  = "workstation-config"
#   workstation_cluster_id = google_workstations_workstation_cluster.default.workstation_cluster_id
#   location               = var.gcp_region
#   project                = var.gcp_project_id

#   host {
#     gce_instance {
#       machine_type                = "e2-standard-2"
#       boot_disk_size_gb           = 35
#       disable_public_ip_addresses = true
#       pool_size                   = 1
#     }
#   }

#   container {
#     image = "us-central1-docker.pkg.dev/genai-alevz-demo-1/cworkstation/custom-workstation:latest"
#   }
# }

# resource "google_workstations_workstation" "default" {
#   provider               = google-beta
#   workstation_id         = "work-station"
#   workstation_config_id  = google_workstations_workstation_config.default.workstation_config_id
#   workstation_cluster_id = google_workstations_workstation_cluster.default.workstation_cluster_id
#   location               = var.gcp_region
#   project                = var.gcp_project_id

#   labels = {
#     "label" = "key"
#   }

#   env = {
#     name = "foo"
#   }

#   annotations = {
#     label-one = "value-one"
#   }
# }

resource "google_redis_instance" "cache" {
  name              = "memory-cache"
  memory_size_gb    = 1
  location_id       = var.gcp_zone
  project           = var.gcp_project_id
  authorized_network = google_compute_network.hol.id
  connect_mode       = "PRIVATE_SERVICE_ACCESS"

  depends_on = [ 
    google_project_service.gcp_services,
    google_service_networking_connection.private_service_connection
  ]
}

# resource "google_artifact_registry_repository" "my-repo" {
#   location      = "us-central1"
#   repository_id = "pyconid"
#   description   = "example docker repository"
#   format        = "DOCKER"
#   provider      = google-beta
#   project       = var.gcp_project_id
# }
#
# resource "google_service_account" "sa" {
#   account_id   = "pyconid-sa"
#   display_name = "A service account for pyconid"
# }

# resource "google_project_iam_member" "sa" {
#   project   = var.gcp_project_id
#   role      = "roles/aiplatform.user"
#   member    = "serviceAccount:${google_service_account.sa.email}"

#   depends_on = [ 
#     google_service_account.sa
#   ]
# }
#

resource "random_id" "db_name_suffix" {
  byte_length = 4
}

resource "google_sql_database_instance" "instance" {
  provider = google-beta

  name             = "private-instance-${random_id.db_name_suffix.hex}"
  region           = var.gcp_region
  database_version = "MYSQL_5_7"
  project          = var.gcp_project_id
  deletion_protection = false

  depends_on = [google_service_networking_connection.private_service_connection]

  settings {
    tier = "db-f1-micro"
    ip_configuration {
      ipv4_enabled                                  = false
      private_network                               = google_compute_network.hol.id
      enable_private_path_for_google_cloud_services = true
      #allocated_ip_range                            = google_compute_global_address.service_range.name
    }
  }
}

resource "google_service_account" "default" {
  account_id   = "service-account-gke"
  display_name = "Service Account"
}

resource "google_container_cluster" "primary" {
  name               = "gke-cluster"
  location           = var.gcp_region
  initial_node_count = 3
  network            = google_compute_network.hol.id
  subnetwork         = google_compute_subnetwork.hol.id
  project            = var.gcp_project_id
  ip_allocation_policy {
    cluster_secondary_range_name  = "pod"
    services_secondary_range_name = "svc"
  }
  node_config {
    # Google recommends custom service accounts that have cloud-platform scope and permissions granted via IAM Roles.
    service_account = google_service_account.default.email
    oauth_scopes = [
      "https://www.googleapis.com/auth/cloud-platform"
    ]
    labels = {
      foo = "bar"
    }
    tags = ["foo", "bar"]
  }
  timeouts {
    create = "30m"
    update = "40m"
  }
}