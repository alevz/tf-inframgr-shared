variable "gcp_project_id" {
  type        = string
  description = "The GCP Project ID to apply this config to."
}
variable "gcp_region" {
  type        = string
  description = "The GCP region to apply this config to."
}
variable "gcp_zone" {
  type        = string
  description = "The GCP zone to apply this config to."
}
variable "gcp_service_list" {
  description ="The list of apis necessary for the project"
  type = list(string)
  default = [
    "run.googleapis.com",
    "redis.googleapis.com",
    "aiplatform.googleapis.com",
    "cloudbuild.googleapis.com",
    "compute.googleapis.com",
    "secretmanager.googleapis.com",
    "workstations.googleapis.com",
    "config.googleapis.com",
    "servicenetworking.googleapis.com",
    "vpcaccess.googleapis.com",
    "cloudresourcemanager.googleapis.com"
  ]
}
